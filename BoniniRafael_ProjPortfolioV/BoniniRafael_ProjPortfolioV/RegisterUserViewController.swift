//
//  RegisterUserViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/22/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class RegisterUserViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var contactName: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var iconTopPage: UIImageView!
    
    var nameTextImageView = UIImageView()
    var contactNameImageView = UIImageView()
    var contactNumberImageView = UIImageView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        navigationItem.title = "New Contact"
        
        continueButton.layer.cornerRadius = 10
        
        setHeightPlaceholderTextColor()

        // Do any additional setup after loading the view.
        
        iconTopPage.tintColor = .white
    }
    
    func setHeightPlaceholderTextColor(){
    
        nameTextField.frame.size.height = 40
        nameTextField.attributedPlaceholder = NSAttributedString(string: "First Name",
                                                                 attributes: [NSAttributedStringKey.foregroundColor: UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.6)])
        
        contactName.frame.size.height = 40
        contactName.attributedPlaceholder = NSAttributedString(string: "Contact Name",
                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.6)])
        
        contactNumber.frame.size.height = 40
        contactNumber.attributedPlaceholder = NSAttributedString(string: "Phone #",
                                                                attributes: [NSAttributedStringKey.foregroundColor: UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.6)])
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.frame = CGRect(x: 0, y: -85, width: self.view.frame.width, height: self.view.frame.height)
        })
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            //            self.view.layoutIfNeeded()
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func confirmInformation(_ sender: Any) {
        
        if let nameTV = nameTextField.text, let emergencyName = contactName.text, let emergencyNumber = contactNumber.text{
            
            
            if(nameTV == "" || nameTV.trimmingCharacters(in: .whitespaces).count == 0){
                
                nameTextField.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
                
            }else if(emergencyName == "" || emergencyName.trimmingCharacters(in: .whitespaces).count == 0){
                
                nameTextField.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                contactName.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
                
            }else if(emergencyNumber == "") || !emergencyNumber.isPhoneNumber{
                
                contactName.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                contactNumber.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
                
            }else{
                
                nameTextField.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                contactName.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                contactNumber.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                print("EVERYTHING IS FINE")
                
                UserDefaults.standard.setValue(nameTV, forKey: "userName")
                
                UserDefaults.standard.setValue( 6,forKey:"sensibility")

                let emergencyContacts : [EmergencyContactsDataModel] = [EmergencyContactsDataModel(name: emergencyName, phoneNumber: emergencyNumber)]
                
                NSKeyedArchiver.setClassName("EmergencyContactsDataModel",for: EmergencyContactsDataModel.self)
                
                let objectarray = NSKeyedArchiver.archivedData(withRootObject: emergencyContacts)
                
                UserDefaults.standard.setValue(objectarray, forKey: "EmergencyContacts")
                
                
                
                dismissCurrentView()
            }
        }
    }
    
    func dismissCurrentView(){
    self.dismiss(animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension String {
    var isPhoneNumber: Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: self, options: [], range: NSMakeRange(0, self.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == self.count
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}

