//
//  EmergencyContactsDataModel.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/22/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class EmergencyContactsDataModel: NSObject, NSCoding {
    
    var name : String?
    var phoneNumber : String?
    
    
    init(name:String, phoneNumber:String?){
        self.name = name
        self.phoneNumber = phoneNumber
    }
 
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey:"nameString")
        aCoder.encode(phoneNumber,forKey:"phoneNumberString")
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        self.init(name: "John",phoneNumber:"+00000000000")
        
        name = aDecoder.decodeObject(forKey: "nameString") as? String
        phoneNumber = aDecoder.decodeObject(forKey: "phoneNumberString") as? String
        
    }
}
