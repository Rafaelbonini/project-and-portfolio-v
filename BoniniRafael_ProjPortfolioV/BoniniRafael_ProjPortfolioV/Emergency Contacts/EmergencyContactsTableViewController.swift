//
//  EmergencyContactsTableViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/22/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

private let addContactIdentifier = "addcontact"
class EmergencyContactsTableViewController: UITableViewController{
    
    
    var allEmergencyContacts : [EmergencyContactsDataModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let data = UserDefaults.standard.value(forKey: "EmergencyContacts") as? Data{
            
            NSKeyedUnarchiver.setClass(EmergencyContactsDataModel.self ,forClassName:"EmergencyContactsDataModel")
            
            if let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? [EmergencyContactsDataModel]{
                
                
                //if allEmergencyContacts has 1 item less than array a new contact has been added
                if allEmergencyContacts.count == array.count-1{
                    //add new row
                    allEmergencyContacts = array
                    
                    tableView.reloadData()
                    
                    let indexPath = IndexPath(row: array.count-1, section: 0)
                    
                    tableView.reloadRows(at: [indexPath], with: .left)
                }else{
                    //nothing added
                    allEmergencyContacts = array
                    
                }
                
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allEmergencyContacts.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        // Configure the cell...
        
        cell.textLabel?.text = allEmergencyContacts[indexPath.row].name
        cell.detailTextLabel?.text = allEmergencyContacts[indexPath.row].phoneNumber
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            print("Deleted")
            
            //only delete item if table has more than 1 item
            if allEmergencyContacts.count != 1{
                
                allEmergencyContacts.remove(at: indexPath.row)
                self.tableView.deleteRows(at: [indexPath], with: .automatic)
                
                //save new array to UserDefaults
                NSKeyedArchiver.setClassName("EmergencyContactsDataModel",for: EmergencyContactsDataModel.self)
                
                let objectarray = NSKeyedArchiver.archivedData(withRootObject: allEmergencyContacts)
                
                UserDefaults.standard.setValue(objectarray, forKey: "EmergencyContacts")
                
            }
        }
    }
}
