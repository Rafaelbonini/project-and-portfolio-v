//
//  AddEmergencyContactViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/23/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class AddEmergencyContactViewController: UIViewController {

    
    @IBOutlet weak var contactNameTextField: UITextField!
    @IBOutlet weak var contactPhoneNumberTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Add Emergency Contact"
//        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addToEmergencyContacts(_ sender: Any) {
        
        
        if let name = contactNameTextField.text, let phone = contactPhoneNumberTextField.text{
            
            if (name == "" || name.trimmingCharacters(in: .whitespaces).count == 0){
                
                contactNameTextField.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
                
            }else if(phone == "") || !phone.isPhoneNumber{
                
                contactNameTextField.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                contactPhoneNumberTextField.backgroundColor = UIColor(red: 255, green: 0, blue: 0, alpha: 0.5)
                
                
            }else{
                contactPhoneNumberTextField.backgroundColor = UIColor(red: 255, green: 255, blue: 255, alpha: 0.79)
                print("ALL GOOD")
            
                
                var allEmergencyContacts : [EmergencyContactsDataModel] = []
                
                //get array of contact users from UserDefaults to save new item
                
                if let data = UserDefaults.standard.value(forKey: "EmergencyContacts") as? Data{
                    
                    NSKeyedUnarchiver.setClass(EmergencyContactsDataModel.self ,forClassName:"EmergencyContactsDataModel")
                    
                    if let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? [EmergencyContactsDataModel]{

                        allEmergencyContacts = array
                        
                    }
                    
                    
                }
                
                allEmergencyContacts.append(EmergencyContactsDataModel(name: name, phoneNumber: phone))
                
                print(allEmergencyContacts)
                
                //save new array to UserDefaults
                NSKeyedArchiver.setClassName("EmergencyContactsDataModel",for: EmergencyContactsDataModel.self)
                
                let objectarray = NSKeyedArchiver.archivedData(withRootObject: allEmergencyContacts)
                
                UserDefaults.standard.setValue(objectarray, forKey: "EmergencyContacts")
            
                
                navigationController?.popViewController(animated: true)
            }
            
            
        }
        
        
    }
    
}
