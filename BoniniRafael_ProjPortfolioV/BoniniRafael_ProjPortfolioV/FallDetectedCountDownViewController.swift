//
//  FallDetectedCountDownViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/19/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import Alamofire
import WatchConnectivity

class FallDetectedCountDownViewController: UIViewController, WCSessionDelegate {

    
    
    
    let accountSID = "AC84052c714c083e65f251120e69ec1c17"
    let authToken = "701cbe1cbb3f5ba5c12c5286e7e77a75"
    
    var getLatitude : Double = 0
    var getLongitude : Double = 0
    
    var count = 25
    var startCount = 0
    var timer:Timer?
    var player: AVAudioPlayer?
    
    var shapeLayer: CAShapeLayer!
    var pulsatingLayer: CAShapeLayer!
    
    let percentageLabel: UILabel = {
        let label = UILabel()
        label.text = "25"
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: 32)
        label.textColor = .white
        return label
    }()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc private func handleEnterForeground() {
        animatePulsatingLayer()
    }
    
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 100, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 20
        layer.fillColor = fillColor.cgColor
        layer.lineCap = kCALineCapRound
        layer.position = view.center
        return layer
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        print("FROM FALL DETECTED CONTROLLLLERa ",getLatitude," " , getLongitude)
        
        view.backgroundColor = UIColor.backgroundColor
        setupCircleLayers()
        
        //set a lower volume from the start so the user does not have the sound blasting from the start
        MPVolumeView.setVolumez(0.3)
        
//        UserDefaults.standard.set(3, forKey: "sensibility")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //start timer with selector
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        
        playSound()
        
        startCount = count
        
        setupNotificationObservers()
        
        setupCircleLayers()
        
        setupPercentageLabel()
        
        //set circle stroke to 0 from the start
        shapeLayer.strokeEnd = 0
        
    }
    
    
    private func setupPercentageLabel() {
        view.addSubview(percentageLabel)
        percentageLabel.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        percentageLabel.center = view.center
    }
    
    private func setupCircleLayers() {
        pulsatingLayer = createCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.pulsatingFillColor)
        view.layer.addSublayer(pulsatingLayer)
        animatePulsatingLayer()
        
        let trackLayer = createCircleShapeLayer(strokeColor: .trackStrokeColor, fillColor: .backgroundColor)
        view.layer.addSublayer(trackLayer)
        
        shapeLayer = createCircleShapeLayer(strokeColor: .outlineStrokeColor, fillColor: .clear)
        
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        shapeLayer.strokeEnd = 0
        view.layer.addSublayer(shapeLayer)
    }
    
    private func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.toValue = 1.5
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        
        pulsatingLayer.add(animation, forKey: "pulsing")
    }
    
    fileprivate func animateCircle() {
        let basicAnimation = CABasicAnimation(keyPath: "strokeEnd")
        
        basicAnimation.toValue = 1
        
        basicAnimation.duration = 50
        
        basicAnimation.fillMode = kCAFillModeForwards
        basicAnimation.isRemovedOnCompletion = false
        
        shapeLayer.add(basicAnimation, forKey: "urSoBasic")
    }
    
    //updating ui based on time left
    @objc func update() {
        
        self.animateCircle()
        
        
        if(count > 0) {
            
            //increate volume of user overtime
            if(count == 15){
                MPVolumeView.setVolumez(0.5)
            }else if(count == 5){
                MPVolumeView.setVolumez(1.0)
            }
            
            
            count -= 1
            
            //percentage of shape layer stoke to fill in UI
            let percentage : CGFloat = CGFloat(((count * 100)/startCount - 100) * -1)
            
            print(percentage/100)
            
            DispatchQueue.main.async {
                //update UI
                self.percentageLabel.text =  String(self.count)
                self.shapeLayer.strokeEnd = percentage/100
            }
            
        }else if(count == 0){
            //runs if no action was taken to top the countdown
            
            count -= 1
            timer?.invalidate()
            self.handleCallforHelp()
            
        }
    }
    
  
    func handleCallforHelp(){
        print(count)
    print("helpppppoaishdugiyfasdivguhaosdua")
        
        var currentUserName = ""
        var allContactsArray : [EmergencyContactsDataModel] = []
        
        if let userNameSaved = UserDefaults.standard.string(forKey: "userName"){
            
            currentUserName = userNameSaved
            
        }
        
        if let data = UserDefaults.standard.value(forKey: "EmergencyContacts") as? Data{
            
            NSKeyedUnarchiver.setClass(EmergencyContactsDataModel.self ,forClassName:"EmergencyContactsDataModel")
            
            if let array = NSKeyedUnarchiver.unarchiveObject(with: data) as? [EmergencyContactsDataModel]{
                
                allContactsArray = array
            }
        }
        
        for contact in allContactsArray{
            
            if let contactNumber = contact.phoneNumber{
                
                let url = "https://api.twilio.com/2010-04-01/Accounts/\(accountSID)/Messages"
                let parameters = ["From": "+17657683233", "To": contactNumber, "Body": "We have detected a fall, \(currentUserName) may need emergency assistance! location: http://www.google.com/maps/place/\(getLatitude),\(getLongitude)"]
                
                Alamofire.request(url, method: .post, parameters: parameters)
                    .authenticate(user: accountSID, password: authToken)
                    .responseJSON { response in
                        debugPrint(response)
                }

            }

        }
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func dismissFallButton(_ sender: Any) {
        timer?.invalidate()
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func playSound() {
        guard let url = Bundle.main.url(forResource: "siren", withExtension: "wav") else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            //set volume to max
            player?.volume = 1.0
            
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.wav.rawValue)
            
            guard let player = player else { return }
            
            //keep looping the sound
            player.numberOfLoops = -1
            
            //continue to play alert sound when chaged form the foreground to background
            let session = AVAudioSession.sharedInstance()
            do{
                try session.setCategory(AVAudioSessionCategoryPlayback)
            }
            catch
            {
                
            }
            
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        if let msge = message["Message"] as? String{
            
            if msge == "Dissmiss_Fall"{
                print(msge)

                    self.timer?.invalidate()
                    self.dismiss(animated: true, completion: nil)
                    

            }
   
        }
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
}
extension UIColor {
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
    
    static let backgroundColor = UIColor.rgb(r: 21, g: 22, b: 33)
    static let outlineStrokeColor = UIColor.rgb(r: 234, g: 46, b: 111)
    static let trackStrokeColor = UIColor.rgb(r: 56, g: 25, b: 49)
    static let pulsatingFillColor = UIColor.rgb(r: 86, g: 30, b: 63)
}
extension MPVolumeView {
    
    static func setVolumez(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.5) {
            slider?.value = volume;
        }
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
