//
//  SettingsUIViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class SettingsUIViewController: UIViewController {

    @IBOutlet weak var changeNameTextField: UITextField!
    @IBOutlet weak var sensibilitySlider: UISlider!
    
    var sliderSelectedSensitivity : Double = 100
    var currentSensibility : Double = 100
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = "Settings"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
//        view.backgroundColor = UIColor.init(hexString: "#381931")
        navigationController?.navigationBar.tintColor = .white
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        currentSensibility = UserDefaults.standard.double(forKey: "sensibility")
        
        
        sensibilitySlider.setValue(Float(currentSensibility), animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func sensibilityChanged(_ sender: UISlider) {
        
        let currentValue = Double(sender.value)
        
        sliderSelectedSensitivity = currentValue
        
        print(currentValue)
    }
    
    @IBAction func saveSetting(_ sender: Any) {
    
        if let changeName = changeNameTextField.text{
            
            if(changeName != "" || changeName.trimmingCharacters(in: .whitespaces).count != 0){
                //change user name
                
                UserDefaults.standard.setValue(changeName, forKey: "userName")
            }
            
            if currentSensibility != sliderSelectedSensitivity{
                
                UserDefaults.standard.setValue(sliderSelectedSensitivity.rounded(toPlaces: 1), forKey: "sensibility")
             
                print("current sensibility settings",sliderSelectedSensitivity.rounded(toPlaces: 1))
            }
            
        }
        
        navigationController?.popViewController(animated: true)
    }
    

}
