//
//  SlideMenu.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/13/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class SlideMenu : NSObject, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    let cellId = "cellId"
    //Options inside slide menu
    var menuOptions : [String] = ["Emergency Contacts","Settings"]
    var viewC: ViewController?

    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let blackView = UIView()
    let userNameLabelView = UIView()
    var label = UILabel()
    
    func createMenuAndShadowView(username: String){
        
    
        if let window = UIApplication.shared.keyWindow{
            
            
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.3)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleMenuDismiss)))
            
            userNameLabelView.backgroundColor = UIColor(hexString: "#5B1621")
            userNameLabelView.frame = CGRect(x: -((window.frame.width/100)*70), y: UIApplication.shared.statusBarFrame.height, width: ((window.frame.width/100)*70), height: 150)
            
            
            window.addSubview(blackView)
            window.addSubview(collectionView)
            window.addSubview(userNameLabelView)
            
            
            label = UILabel(frame: CGRect(x: 10, y: 150, width: userNameLabelView.frame.width, height: 100))
            label.center = CGPoint(x: userNameLabelView.frame.width/2, y: userNameLabelView.frame.maxY-60)
            label.textAlignment = .center
            label.font = UIFont(name: "Helvetica Neue", size: 30)
            label.text = username
            label.textColor = .white
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            
            userNameLabelView.addSubview(label)
            
            
            
            collectionView.frame = CGRect(x: -((window.frame.width/100)*70), y: UIApplication.shared.statusBarFrame.height + 150, width: ((window.frame.width/100)*70), height: window.frame.height)
            
            
            blackView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: window.frame.width, height: window.frame.height)
            blackView.alpha = 0
            
            
            let topCollectionShadow = EdgeShadowLayer(forView: collectionView, edge: .Top)
            collectionView.layer.addSublayer(topCollectionShadow)
            

            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.blackView.alpha = 1
                
                self.blackView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: window.frame.width, height: window.frame.height)
                
                self.collectionView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height + 150, width: (window.frame.width/100)*70, height: window.frame.height)
                
                self.userNameLabelView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: (window.frame.width/100)*70, height: 150)
                
                
            }, completion: nil)
   
            
        }
    }
    
    @objc func handleMenuDismiss(){
        //animation when clicked on the blackview
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
            
            //hide translucid view
            self.blackView.alpha = 0
            
            if let window = UIApplication.shared.keyWindow{
                // move collection view left and out of screen
                self.collectionView.frame = CGRect(x: -(window.frame.width/100)*60 , y: UIApplication.shared.statusBarFrame.height+150, width: (window.frame.width/100)*60, height: window.frame.height)
                
                //move transclucid view with collection view
                self.blackView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height , width: window.frame.width, height: window.frame.height)
                
                //hide view with the user name label to the left
                self.userNameLabelView.frame = CGRect(x: -((window.frame.width/100)*60), y: UIApplication.shared.statusBarFrame.height, width: ((window.frame.width/100)*60), height: 150)
                
            }
            
        }, completion: nil)
        
        label.removeFromSuperview()
        
    }
    

    //MARK: COLLECTION VIEW CELLS SETUP
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    //number of cells
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return menuOptions.count
    }
    
    //cells labels
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SlideMenuCell
        
        cell.nameLabel.text = menuOptions[indexPath.row]
        

        return cell
    }
    
    //cells size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    //space between cells
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    //cell was selected
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 0
                if let window = UIApplication.shared.keyWindow{
                    self.collectionView.frame = CGRect(x: -(window.frame.width/100)*60 , y:UIApplication.shared.statusBarFrame.height + 150 , width: (window.frame.width/100)*60, height: window.frame.height)
                    
                    self.blackView.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height , width: window.frame.width, height: window.frame.height)
                    
                    self.userNameLabelView.frame = CGRect(x: -((window.frame.width/100)*60), y: UIApplication.shared.statusBarFrame.height, width: ((window.frame.width/100)*60), height: 150)
                    
                }
            }) { (completion) in
                
                self.viewC?.showSettingsController(menuOption: self.menuOptions[indexPath.row])
   
                self.label.removeFromSuperview()
            }
    }
    
    override init() {
        super.init()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(SlideMenuCell.self, forCellWithReuseIdentifier: cellId)
    }
}
