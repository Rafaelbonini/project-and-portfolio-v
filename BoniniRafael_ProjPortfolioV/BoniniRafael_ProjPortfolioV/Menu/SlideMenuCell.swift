//
//  SlideMenuCell.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/13/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class SlideMenuCell: UICollectionViewCell {
    
    
     override var isHighlighted: Bool{
        didSet{
            print(" que")
            backgroundColor = isHighlighted ? UIColor.init(hexString: "#489BC4") : UIColor.white
        }
    }
    
    let nameLabel : UILabel = {
        let label = UILabel()
        label.text = "testing"
        label.textAlignment = NSTextAlignment.center
        label.font = UIFont(name: "Helvetica Neue", size: 20)
//        label.backgroundColor = UIColor.gray
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        addSubview(nameLabel)
        
        addConstraintsWithFormat("H:|-10-[v0]-10-|", views: nameLabel)
        addConstraintsWithFormat("V:|[v0]|", views: nameLabel)
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented ")
    }
}

extension UIView {
    func addConstraintsWithFormat(_ format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
