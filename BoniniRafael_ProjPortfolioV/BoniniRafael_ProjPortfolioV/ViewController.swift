//
//  ViewController.swift
//  BoniniRafael_ProjPortfolioV
//
//  Created by Rafael Bonini on 8/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation
import UserNotifications
import AVFoundation
import WatchConnectivity

private let SettingsVCIdentifier = "settings"
private let FallCountDownIdentifier = "countdown"
private let RegisterUserVCIdentifier = "registeruser"
private let EmergencyContactsTCIdentifier = "EmergencyTable"

class ViewController: UIViewController,CLLocationManagerDelegate, WCSessionDelegate{
    
    var player: AVAudioPlayer?
    @IBOutlet weak var label: UILabel!
    var highestValue : Double = 0.0
    var currentLatitude : Double = 0
    var currentLongitude : Double = 0
    var currentUserName = ""
    var currentSentibilityLevel : Double = 1000
    var lastMessage: CFAbsoluteTime = 0
    
    
    var motionM : CMMotionManager = CMMotionManager();
    
    var locationM : CLLocationManager = CLLocationManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        

        
        if let userNameSaved = UserDefaults.standard.string(forKey: "userName"){
        currentUserName = userNameSaved
            
        }

        if currentUserName == "" {
            performSegue(withIdentifier: RegisterUserVCIdentifier, sender: self)
        }
    }
    
    //toggle menu
    @IBAction func menuTapped(_ sender: Any) {
        
        
        displayMenu()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.badge,.sound], completionHandler: {didAlow, error in
        })
        
        navigationItem.title = "Fallp"
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        if let userNameSaved = UserDefaults.standard.string(forKey: "userName"){
            
            currentUserName = userNameSaved
            
            
        }
        
        let sensibility = UserDefaults.standard.double(forKey: "sensibility")
        print("sensibility" ,sensibility)
        
        if sensibility != 0{
            currentSentibilityLevel = sensibility
        }
        
        
        
        //register custom audio in bundle to play in notification
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
        }catch let error {
            print(error.localizedDescription)
        }
        
        
        //MARK: - REQUEST USER AUTHORIZATION FOR LOCATION
        let status = CLLocationManager.authorizationStatus()
        if status == .denied || status == .restricted{
            
            return
        }
        
        locationM.delegate = self
        
        if status == .notDetermined{
            locationM.requestAlwaysAuthorization()
            
        } else if status == .denied{
            locationM.requestAlwaysAuthorization()
            
        }else if status == .authorizedAlways || status == .authorizedWhenInUse{
            
            if CLLocationManager.locationServicesEnabled(){
                locationM.startUpdatingLocation()
            }
        }
        
        locationM.allowsBackgroundLocationUpdates = true
        locationM.pausesLocationUpdatesAutomatically = false
        
        
        //set nav controler color
        navigationController?.navigationBar.barTintColor = UIColor.init(hexString: "#8C1931")
    }
    
    
    func displayCountDownController(){
        performSegue(withIdentifier: FallCountDownIdentifier, sender: self)
    }
    
    
    func showSettingsController(menuOption: String){
        //        let settingsController = SettingsUIViewController()
        
        
        if menuOption == "Settings" {
            performSegue(withIdentifier: SettingsVCIdentifier , sender: self)
        }else if(menuOption == "Emergency Contacts"){
            performSegue(withIdentifier: EmergencyContactsTCIdentifier , sender: self)
        }
        
        
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let segueId = segue.identifier
        
        if segueId == FallCountDownIdentifier {
            
            let fallCountVC = segue.destination as! FallDetectedCountDownViewController
            
            fallCountVC.getLatitude = currentLatitude
            fallCountVC.getLongitude = currentLongitude
            
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        //
    }
    
    //lazy var to instantiate View controller only once
    lazy var lauchMenu: SlideMenu = {
        let menu = SlideMenu()
        menu.viewC = self
        return menu
    }()
    
    func displayMenu(){
        
        lauchMenu.createMenuAndShadowView(username: currentUserName)
        
    }
    
    func notification(){
        let content = UNMutableNotificationContent()
        content.title = "FALL DETECTED"
        //        content.subtitle = "Tap here to cancel"
        content.body = "Tap here to cancel"
        content.badge = 1
        
        
        content.sound = UNNotificationSound(named: "siren.wav")
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.1, repeats: false)
        let identifier = "Reminder-blablabla"
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        label.text = "Fall Detection Activated"
        
        //start getting device motion updates to calculate device acceleration
        motionM.deviceMotionUpdateInterval = 1.0/50.0
        motionM.startDeviceMotionUpdates(to: OperationQueue.main) { (deviceMotion, error) in
            if let error = error{
                print(error)
                return
            }
            let userAcceletarion:CMAcceleration = (deviceMotion?.userAcceleration)!
            let totalacceleration:Double = sqrt(userAcceletarion.x * userAcceletarion.x+userAcceletarion.y*userAcceletarion.y+userAcceletarion.z*userAcceletarion.z)
            //                        print("Total acceleration: ",totalacceleration)
            
            if(totalacceleration > self.highestValue){
                self.highestValue = totalacceleration
            }
            
            //if device reaches selected acceleration, consider a possible fall has ocurred
            if(totalacceleration > self.currentSentibilityLevel){
                
                self.sendWatchMessage()
                print("current sensibility level: ", self.currentSentibilityLevel)
                
                self.currentLatitude = locValue.latitude
                self.currentLongitude = locValue.longitude
                
                self.displayCountDownController()
                
                self.notification()
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("wild appears")
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func sendWatchMessage() {
        let currentTime = CFAbsoluteTimeGetCurrent()
        
        // if less than half a second has passed, bail out
        if lastMessage + 0.5 > currentTime {
            return
        }
        
        // send a message to the watch if it's reachable
        if (WCSession.default.isReachable) {
            // this is a meaningless message, but it's enough for our purposes
            let message = ["Message": "Possible_Fall"]
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
        
        // update our rate limiting property
        lastMessage = CFAbsoluteTimeGetCurrent()
    }
}

//extension to convert hex to ui color
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
