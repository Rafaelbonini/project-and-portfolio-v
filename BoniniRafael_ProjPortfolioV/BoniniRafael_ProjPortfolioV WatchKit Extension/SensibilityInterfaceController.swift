//
//  SensibilityInterfaceController.swift
//  BoniniRafael_ProjPortfolioV WatchKit Extension
//
//  Created by Rafael Bonini on 8/23/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import WatchKit
import Foundation


class SensibilityInterfaceController: WKInterfaceController {

    @IBOutlet var sensibilitySlider: WKInterfaceSlider!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        
        let watchSensibility = UserDefaults.standard.float(forKey: "watchSensibility")
        
        if watchSensibility != 0{
            
            sensibilitySlider.setValue(watchSensibility)
        }
    }
    
    @IBAction func valueChanged(_ value: Float) {

        UserDefaults.standard.set(value, forKey: "watchSensibility")
    }
    

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
