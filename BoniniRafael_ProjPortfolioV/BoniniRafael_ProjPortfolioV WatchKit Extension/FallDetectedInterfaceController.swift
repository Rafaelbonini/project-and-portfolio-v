//
//  FallDetectedInterfaceController.swift
//  BoniniRafael_ProjPortfolioV WatchKit Extension
//
//  Created by Rafael Bonini on 8/23/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class FallDetectedInterfaceController: WKInterfaceController, WCSessionDelegate {
    
    var lastMessage: CFAbsoluteTime = 0

    let duration:Double = 25
    
    @IBOutlet var timer: WKInterfaceTimer!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        

        
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        if (WCSession.isSupported()) {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        
        
        
        timer.setDate(NSDate(timeIntervalSinceNow: duration ) as Date)
        timer.start()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    @IBAction func dismissCounter() {
        sendWatchMessage()
        
        self.dismiss()
        
    }
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sendWatchMessage() {
        let currentTime = CFAbsoluteTimeGetCurrent()
        
        // if less than half a second has passed, bail out
        if lastMessage + 0.5 > currentTime {
            return
        }
        
        // send a message to the watch if it's reachable
        if (WCSession.default.isReachable) {
            // this is a meaningless message, but it's enough for our purposes
            let message = ["Message": "Dissmiss_Fall"]
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
        
        // update our rate limiting property
        lastMessage = CFAbsoluteTimeGetCurrent()
    }
    
}
