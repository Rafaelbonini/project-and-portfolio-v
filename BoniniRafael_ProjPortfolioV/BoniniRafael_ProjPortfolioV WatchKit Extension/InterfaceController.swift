//
//  InterfaceController.swift
//  BoniniRafael_ProjPortfolioV WatchKit Extension
//
//  Created by Rafael Bonini on 8/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import WatchKit
import Foundation
import CoreMotion
import AVFoundation
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate{
    
    
    
    @IBOutlet var highestValues: WKInterfaceLabel!
    
    var motionManager = CMMotionManager()
    
    var highestValue : Double = 0.0
    
    var player: AVAudioPlayer?
    
    var locationManager : CLLocationManager = CLLocationManager()
    
    var sensibility : Double = 6

    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
    
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        
        let watchSensibility = UserDefaults.standard.double(forKey: "watchSensibility")
        
        if watchSensibility != 0{
            
            sensibility = watchSensibility
        }
            
        
        motionManager.deviceMotionUpdateInterval = 1.0/50.0
        
        
        motionManager.startDeviceMotionUpdates(to: OperationQueue.main) { (deviceMotion, error) in
            if let error = error{
                print(error)
                return
            }
            
            let userAcceletarion:CMAcceleration = (deviceMotion?.userAcceleration)!
            let totalacceleration:Double = sqrt(userAcceletarion.x * userAcceletarion.x+userAcceletarion.y*userAcceletarion.y+userAcceletarion.z*userAcceletarion.z)
            
            if(totalacceleration > self.sensibility){
                
                self.playSound()
                
                
            }
            if(totalacceleration > self.highestValue){
                self.highestValue = totalacceleration
                self.highestValues.setText(String(format:"%.1f", self.highestValue))
            }
            
            
            
        }
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    func playSound() {

        if let path = Bundle.main.path(forResource: "siren", ofType: "wav") {
            
            let fileUrl = URL(fileURLWithPath: path)
            
            do{
                try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
                try AVAudioSession.sharedInstance().setActive(true)
                
                player = try AVAudioPlayer(contentsOf: fileUrl)
                
                guard let player = player else { return }
                
                let session = AVAudioSession.sharedInstance()
                do{
                    try session.setCategory(AVAudioSessionCategoryPlayback)
                }
                catch
                {
                    
                }
                player.play()
                
            }
            catch
            {
                
            }
            
        }
        
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        if let msge = message["Message"] as? String{
            
            if msge == "Possible_Fall"{
                print(msge)
                
                DispatchQueue.main.async {
                    self.presentController(withName: "falldetected", context: self)
                }
                
                
                
            }
            
            
        }
        
        
        
    }
    
}



